export class Itinerario {
    lat: string;
    lng: string;
    ponto: number;
  idlinha: string;

    constructor(lat: string, lng: string, ponto: number) {
        this.lat = lat;
        this.lng = lng;
        this.ponto = ponto + 1;
    }

}
