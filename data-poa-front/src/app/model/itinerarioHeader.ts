export class ItinerarioHeader {
    idlinha: string;
    nome: string;
    codigo: string;

    constructor(idlinha: string, nome: string, codigo: string){
        this.idlinha = idlinha;
        this.nome = nome;
        this.codigo = codigo;
    }
}