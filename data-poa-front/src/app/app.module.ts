import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LinhasOnibusComponent } from './components/linhas-onibus/linhas-onibus.component';
import { AppRoutingModule } from './routes/app-routing.module';
import { LinhasLotacaoComponent } from './components/linhas-lotacao/linhas-lotacao.component';
import { HttpClientModule } from '@angular/common/http';
import { ItinerarioComponent } from './components/itinerario/itinerario.component';
import { Filtro } from './pipes/filtro.pipes';
import { AppBootstrapModule } from './components/app-bootstrap/app-bootstrap.module';

@NgModule({
  declarations: [
    AppComponent,
    LinhasOnibusComponent,
    LinhasLotacaoComponent,
    ItinerarioComponent,
    Filtro
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppBootstrapModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
