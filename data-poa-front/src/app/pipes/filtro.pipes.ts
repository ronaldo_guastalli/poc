import { Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'filtro' })
export class Filtro implements PipeTransform {
  transform(dados: any[], textoPesquisa: string): any[] {
    return dados !== undefined ? (
      dados.filter((texto: any) => texto.nome.toLowerCase()
        .includes(textoPesquisa.toLowerCase())
        || texto.codigo.toLowerCase().includes(textoPesquisa.toLowerCase())
        || texto.id.includes(textoPesquisa))) : [];
  }
}