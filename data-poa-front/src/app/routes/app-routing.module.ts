import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { LinhasOnibusComponent }      from '../components/linhas-onibus/linhas-onibus.component';
import { LinhasLotacaoComponent } from '../components/linhas-lotacao/linhas-lotacao.component';
import { ItinerarioComponent } from '../components/itinerario/itinerario.component';

const routes: Routes = [
  { path: 'linhas-onibus', component: LinhasOnibusComponent },
  { path: 'linhas-lotacao', component: LinhasLotacaoComponent },
  { path: 'itinerario/:id', component: ItinerarioComponent }
];
@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ]
})

export class AppRoutingModule { }
