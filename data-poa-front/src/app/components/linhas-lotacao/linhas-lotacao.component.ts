import { Component, OnInit } from '@angular/core';
import { LinhasLotacaoService } from '../../service/linhasLotacao.service';
import { Linha } from '../../model/linha';

@Component({
  selector: 'app-linhas-lotacao',
  templateUrl: './linhas-lotacao.component.html',
  providers: [LinhasLotacaoService],
  styleUrls: ['./linhas-lotacao.component.css']
})
export class LinhasLotacaoComponent implements OnInit {
  linhasLotacao: Linha[];
  textoPesquisa: string = '';
  constructor(private service: LinhasLotacaoService) {
  }

  ngOnInit() {
    this.getListaLotacao();
  }


  getListaLotacao() {
    this.service.getListaLotacoes()
      .subscribe(linhasLotacao =>
        this.linhasLotacao = linhasLotacao);
  }

}
