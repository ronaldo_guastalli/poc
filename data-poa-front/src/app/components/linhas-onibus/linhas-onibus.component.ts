import { Component, OnInit, TemplateRef } from '@angular/core';
import { LinhasOnibusService } from '../../service/linhasOnibus.service';
import { Linha } from '../../model/linha';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-linhas-onibus',
  templateUrl: './linhas-onibus.component.html',
  providers: [LinhasOnibusService, BsModalService],
  styleUrls: ['./linhas-onibus.component.css']
})
export class LinhasOnibusComponent implements OnInit {
  linhasOnibus: Linha[];
  textoPesquisa = '';
  public modalRef: BsModalRef;
  id: string;
  constructor(private service: LinhasOnibusService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getListaLotacao();
  }
  getListaLotacao() {
   this.service.getListaLotacoes()
   .subscribe(linhasOnibus => this.linhasOnibus = linhasOnibus);
  }

  public openModal(template: TemplateRef<any>, id: string) {
    id = id;
    console.log(id);
    this.modalRef = this.modalService.show(template);
  }


}
