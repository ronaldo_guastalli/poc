import { Component, OnInit } from '@angular/core';
import { ItinerarioService } from '../../service/itinerario.service';
import { Itinerario } from '../../model/itinerario';
import { ActivatedRoute } from '@angular/router';
import { ItinerarioHeader } from '../../model/itinerarioHeader';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  providers: [ItinerarioService],
  styleUrls: ['./itinerario.component.css'],
})
export class ItinerarioComponent implements OnInit {
  itinerario: Itinerario[] = new Array();
  itinerarioHeader: ItinerarioHeader;
  id: number = +this.route.snapshot.paramMap.get('id');

  idTeste: any;

  constructor(
    private service: ItinerarioService,
    private route: ActivatedRoute
  ) { }


  ngOnInit() {
    this.getItinerario();
  }


  getItinerario(): void {
    // tslint:disable-next-line:radix
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(this.idTeste);
    this.service.getItinerario(id).subscribe(retorno => {
      let i = 0;
      while (retorno[i] !== undefined) {
        this.itinerario.push(new Itinerario(retorno[i].lat, retorno[i].lng, i));
        i++;
      }

      if ( retorno !== undefined) {
        this.itinerarioHeader = new ItinerarioHeader(
          retorno['idlinha'],
          retorno['nome'] !== undefined ? retorno['nome'] : 'nome não informado',
          retorno['codigo']);
      }

    });
  }

}
