import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Linha } from '../model/linha';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LinhasLotacaoService {

  linhasLotacaoUrl = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l'; // URL to web api

  constructor(private http: HttpClient) { }

  /** GET listaLotacoes da api */
  public getListaLotacoes(): Observable<Linha[]> {
    return this.http.get<Linha[]>(this.linhasLotacaoUrl)
      .pipe();
  }
}
