import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Linha } from '../model/linha';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LinhasOnibusService {

  linhasOnibusUrl: string = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%25&t=o'; // URL to web api

  constructor(private http: HttpClient) { }

  /** GET listaLotacoes da api */
  public getListaLotacoes(): Observable<Linha[]> {
    return this.http.get<Linha[]>(this.linhasOnibusUrl)
      .pipe();
  }
}
