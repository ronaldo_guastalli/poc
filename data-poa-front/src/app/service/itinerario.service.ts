import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Itinerario } from '../model/itinerario';

@Injectable()
export class ItinerarioService {
    constructor(private http: HttpClient) { }

    public getItinerario(id: number): Observable<Itinerario> {
        return this.http.get<Itinerario>(`http://www.poatransporte.com.br/php/facades/process.php?a=il&p=${id}`).pipe();
    }


}