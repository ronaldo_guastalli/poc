package br.com.dbccompany.datapoa.repository;

import br.com.dbccompany.datapoa.entity.Itinerario;
import br.com.dbccompany.datapoa.entity.Linha;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ItinerarioRepository extends CrudRepository<Itinerario, Long> {

    public Itinerario findByLinha(Linha linha);


}
