package br.com.dbccompany.datapoa.entity;

import br.com.dbccompany.datapoa.enums.TipoLinha;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "LINHA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Linha.class)
public class Linha {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "NOME")
    private String nome;

    @OneToOne(mappedBy = "linha")
    private Itinerario itinerario;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_LINHA")
    private TipoLinha tipoLinha;

    @ManyToMany(mappedBy = "linhas")
    private List<Cliente> clientes = new ArrayList<>();

    public Linha() {
    }

    public Linha(Long id, String codigo, String nome, TipoLinha tipoLinha) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
        this.tipoLinha = tipoLinha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Itinerario getItinerario() {
        return itinerario;
    }

    public void setItinerario(Itinerario itinerario) {
        this.itinerario = itinerario;
    }

    public TipoLinha getTipoLinha() {
        return tipoLinha;
    }

    public void setTipoLinha(TipoLinha tipoLinha) {
        this.tipoLinha = tipoLinha;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll(Arrays.asList(clientes));
    }
}
