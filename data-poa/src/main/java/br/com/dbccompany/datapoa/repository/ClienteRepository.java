package br.com.dbccompany.datapoa.repository;

import br.com.dbccompany.datapoa.entity.Cliente;
import br.com.dbccompany.datapoa.entity.Linha;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    Cliente findByCpf(String cpf);

}
