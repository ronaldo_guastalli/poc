package br.com.dbccompany.datapoa.exceptions;

public class MsgException {
        public static final String NOME_INFORMAR = "O nome precisa ser informado!";
        public static final String CPF_INFORMAR = "É preciso informar um cpf válido!";
        public static final String CLIENTE_NAO_ENCONTRADO= "Cliente não existe ou não foi encontrado!";
        public static final String LINHA_CADASTRADA = "Linha já cadastrada para o cliente.";
}
