package br.com.dbccompany.datapoa.DTO;

public class CoordenadaDTO {

    private Long lat;

    private Long lng;

    public CoordenadaDTO(Long lat, Long lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Long getLat() {
        return lat;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Long getLng() {
        return lng;
    }

    public void setLng(Long lng) {
        this.lng = lng;
    }
}
