package br.com.dbccompany.datapoa.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ITINERARIO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Itinerario.class)
public class Itinerario {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "itinerario_seq", sequenceName = "itinerario_seq")
    @GeneratedValue(generator = "itinerario_seq", strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne
    @JoinColumn(name = "LINHA_ID")
    private Linha linha;

    @Column(name = "COORDENADAS", columnDefinition="CLOB")
    private String coordenadas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    @Override
    public boolean equals(Object o) {
        Linha linha = (Linha) o;
        return linha.getId().equals(linha.getId());
    }
}
