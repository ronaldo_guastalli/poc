package br.com.dbccompany.datapoa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DataPoaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataPoaApplication.class, args);
    }

}
