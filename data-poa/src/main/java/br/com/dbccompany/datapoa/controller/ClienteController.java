package br.com.dbccompany.datapoa.controller;


import br.com.dbccompany.datapoa.entity.Cliente;
import br.com.dbccompany.datapoa.entity.Linha;
import br.com.dbccompany.datapoa.exceptions.MsgException;
import br.com.dbccompany.datapoa.repository.LinhaRepository;
import br.com.dbccompany.datapoa.service.ClienteService;
import br.com.dbccompany.datapoa.service.LinhaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private LinhaService linhaService;

    @Autowired
    private LinhaRepository linhaRepository;


    @GetMapping(value = "/")
    @ResponseBody
    public List<Cliente> todosOsClientes() {
        return clienteService.todosOsClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Cliente novoCliente(@RequestBody Cliente cliente) throws Exception {
        return clienteService.salvarCliente(cliente);
    }

    @DeleteMapping(value = "deletar/{id}")
    public ResponseEntity<Cliente> deletarClienteId(@PathVariable Long id) throws Exception {
        clienteService.deletarCliente(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/atualizar/{id}")
    @ResponseBody
    public Cliente atualizarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
        return clienteService.editarCliente(id, cliente);
    }

    @PostMapping(value = "/minhasLinhas")
    @ResponseBody
    public Cliente cadastrarMinhasLinhas(@RequestBody Cliente cliente) {
        return clienteService.editarCliente(cliente.getId(), cliente);
    }


}
