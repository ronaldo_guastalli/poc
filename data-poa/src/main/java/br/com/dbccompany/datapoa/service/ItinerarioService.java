package br.com.dbccompany.datapoa.service;

import br.com.dbccompany.datapoa.entity.Itinerario;
import br.com.dbccompany.datapoa.entity.Linha;
import br.com.dbccompany.datapoa.repository.ItinerarioRepository;
import br.com.dbccompany.datapoa.repository.LinhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

@Service
public class ItinerarioService {

    @Autowired
    private ItinerarioRepository itinerarioRepository;

    @Autowired
    private LinhaRepository linhaRepository;

    @Autowired
    private UtilService utilService;


    public Itinerario buscarItinerario(Long idLinha){
        Linha linha = linhaRepository.findById(idLinha).get();
        if(existeItinerario(linha) == null){
            Itinerario itinerario = formatarItinerario(idLinha, utilService.selecionarCoordenadas(linha.getId()));
            Linha linhaVolta = linhaRepository.findByCodigo(codigoDaVolta(linha));
            Itinerario itinerarioVolta = formatarItinerario(linhaVolta.getId(),utilService.selecionarCoordenadas(linhaVolta.getId()));
            itinerarioRepository.save(itinerarioVolta);
            return itinerarioRepository.save(itinerario);
        }
        return existeItinerario(linha);
    }

    public Itinerario existeItinerario(Linha linha){
        List<Itinerario> itinerarios = (List<Itinerario>) itinerarioRepository.findAll();
        for (Itinerario itinerario : itinerarios  ) {
            if(itinerario.getLinha().equals(linha)) return itinerario;
        }
        return null;
    }

    private Itinerario formatarItinerario(Long idLinha, String coordenadas){
        Itinerario itinerario = new Itinerario();
        Linha linhaDoItinerario = linhaRepository.findById(idLinha).get();
        itinerario.setCoordenadas(coordenadas);
        itinerario.setLinha(linhaDoItinerario);
        return itinerario;
    }

    //codigo indentifica ida ou volta 250-1 e 250-2 para a linha id: 5529 e id: 5530
    private String codigoDaVolta(Linha linha){
        StringBuilder sb = new StringBuilder();
        String[] codigoIda = linha.getCodigo().split("-");
        try{
            if(parseInt(codigoIda[1]) == 1){
                sb.append(codigoIda[0]).append("-2");
            }else{
                sb.append(codigoIda[0]).append("-1");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return sb.toString();
    }

    public List<Itinerario> todosOsItinerarios() {
        return (List<Itinerario>) itinerarioRepository.findAll();
    }
}
