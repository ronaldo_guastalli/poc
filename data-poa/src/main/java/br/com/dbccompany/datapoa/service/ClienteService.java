package br.com.dbccompany.datapoa.service;

import br.com.dbccompany.datapoa.entity.Cliente;
import br.com.dbccompany.datapoa.entity.Linha;
import br.com.dbccompany.datapoa.exceptions.MsgException;
import br.com.dbccompany.datapoa.repository.ClienteRepository;
import br.com.dbccompany.datapoa.repository.LinhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;


    @Autowired
    private LinhaRepository linhaRepository;

    public List<Cliente> todosOsClientes(){
        return (List<Cliente>) clienteRepository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvarCliente(Cliente cliente) throws Exception {
        if(cliente.getCpf() == null){
            throw new Exception(MsgException.CPF_INFORMAR);
        }
        if(cliente.getNome() == null){
            throw new Exception(MsgException.NOME_INFORMAR);
        }
        if(!clienteExiste(cliente))
            return clienteRepository.save(cliente);
        return cliente;
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editarCliente(Long id, Cliente cliente){
        cliente.setId(id);
        return clienteRepository.save(cliente);
    }

    private Cliente clientePossuiAsLinhasCadastradas(Cliente cliente) {
            List<Linha> linhas = cliente.getLinhas();
            List<Linha> linhasDoCliente = clienteRepository.findById(cliente.getId()).get().getLinhas();
            if(linhas == null){
                return cliente;
            }
            if(linhasDoCliente.contains(linhas))
                return null;
            return cliente;
    }

    public void deletarCliente(Long id) throws Exception {
        if(clienteRepository.findById(id).get() != null)
            clienteRepository.deleteById(id);
        else
            throw new Exception(MsgException.CLIENTE_NAO_ENCONTRADO);
    }

    private boolean clienteExiste(Cliente cliente) {
        if(cliente.equals(clienteRepository.findByCpf(cliente.getCpf())))
            return true;
        return false;
    }


}
