package br.com.dbccompany.datapoa.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItinerarioDTO {

    private String idlinha;
    private String nome;
    private String codigo;
    private List<CoordenadaDTO> pontos;



    public String getIdlinha() {
        return idlinha;
    }

    public void setIdlinha(String idlinha) {
        this.idlinha = idlinha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<CoordenadaDTO> getPontos() {
        return pontos;
    }

    public void setPontos(List<CoordenadaDTO> pontos) {
        this.pontos = pontos;
    }
}
