package br.com.dbccompany.datapoa.repository;

import br.com.dbccompany.datapoa.entity.Cliente;
import br.com.dbccompany.datapoa.entity.Linha;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinhaRepository extends CrudRepository<Linha, Long> {

    public List<Linha> findAllByNome(String nome);
    public List<Linha> findAll();
    public Linha findByCodigo(String codigo);
    List<Linha> findAllByNomeContains(String nome);
    List<Linha> findAllByClientes(List<Cliente> clientes);

}
