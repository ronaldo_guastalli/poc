package br.com.dbccompany.datapoa.service;

import br.com.dbccompany.datapoa.DTO.LinhaDTO;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class UtilService {

    public List<LinhaDTO> linhasDTO() {
        return Arrays.asList(configuraRestTemplate().getForObject(
                "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o",
                LinhaDTO[].class));
    }

    private RestTemplate configuraRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
        return restTemplate;
    }

    public String selecionarCoordenadas(Long id) {
        String dados = configuraRestTemplate().getForObject(
                ("http://www.poatransporte.com.br/php/facades/process.php?a=il&p=" + id),
                String.class);
        return formatarCoordenadas(dados);
    }

    private String formatarCoordenadas(String dados) {
        final String regex = ",\"0+\"";
        String coordenadas = "\"0\""+dados.split(regex)[1];
        return coordenadas;
    }

}
