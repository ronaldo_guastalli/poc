package br.com.dbccompany.datapoa.service;

import br.com.dbccompany.datapoa.DTO.LinhaDTO;
import br.com.dbccompany.datapoa.entity.Linha;
import br.com.dbccompany.datapoa.enums.TipoLinha;
import br.com.dbccompany.datapoa.repository.ItinerarioRepository;
import br.com.dbccompany.datapoa.repository.LinhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LinhaService {

    @Autowired
    private LinhaRepository linhaRepository;
    @Autowired
    private ItinerarioRepository itinerarioRepository;

    public List<Linha> buscarLinhas(){
        return linhaRepository.findAll();
    }


    @Transactional( rollbackFor = Exception.class )
    public List<Linha> buscarLinhaPorNome(String nome){
        return linhaRepository.findAllByNomeContains(nome);
    }

    public Linha buscarLinhaPorId(Long id){
        Linha linha = linhaRepository.findById(id).get();
        linha.setItinerario(itinerarioRepository.findByLinha(linha));
        return linha;
    }

    @Transactional( rollbackFor = Exception.class )
    public void salvar(List<LinhaDTO> linhas){
        linhaRepository.saveAll(
                linhas.stream()
                .map(e -> new Linha(Long.parseLong(e.getId()), e.getCodigo(), e.getNome(), TipoLinha.LINHA_ONIBUS))
                .collect(Collectors.toList())
        );
    }


}
