package br.com.dbccompany.datapoa.controller;

import br.com.dbccompany.datapoa.entity.Itinerario;
import br.com.dbccompany.datapoa.service.ItinerarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/itinerario")
public class ItinerarioController {

    @Autowired
    private ItinerarioService itinerarioService;

    @GetMapping(value = "/buscarItinerario/{id}")
    @ResponseBody
    public Itinerario buscarItinerario(@PathVariable Long id) {
        return itinerarioService.buscarItinerario(id);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Itinerario> buscarItinerario() {
        return itinerarioService.todosOsItinerarios();
    }


}
