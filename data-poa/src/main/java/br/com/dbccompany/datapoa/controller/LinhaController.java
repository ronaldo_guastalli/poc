package br.com.dbccompany.datapoa.controller;

import br.com.dbccompany.datapoa.entity.Linha;
import br.com.dbccompany.datapoa.service.LinhaService;
import br.com.dbccompany.datapoa.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@Controller
@RequestMapping("/api/linha")
public class LinhaController {

    @Autowired
    private LinhaService linhaService;

    @Autowired
    private UtilService utilService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Linha> buscarLinhas() {
        return linhaService.buscarLinhas();
    }

    @PostMapping(value = "/novasLinhasApi")
    @ResponseBody
    public String novaLinhasDaApi() {
        linhaService.salvar(utilService.linhasDTO());
        return "OK";
    }

    @GetMapping(value = "/buscar/{id}")
    @ResponseBody
    public Linha buscarLinhaPorId(@PathVariable Long id){
        return linhaService.buscarLinhaPorId(id);
    }

    @GetMapping(value = "/{linhaNome}")
    @ResponseBody
    public List<Linha> buscarLinhaPorNome(@PathVariable String linhaNome){
        return linhaService.buscarLinhaPorNome(linhaNome);
    }


}



