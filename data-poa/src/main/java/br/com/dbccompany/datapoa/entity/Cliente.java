package br.com.dbccompany.datapoa.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "CLIENTE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Cliente.class)
public class Cliente {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "cliente_seq", sequenceName = "cliente_seq")
    @GeneratedValue(generator = "cliente_seq", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CPF")
    private String cpf;

    @ManyToMany
    @JoinTable(name = "CLIENTE_LINHA",
            joinColumns
                    = {@JoinColumn(name = "ID_CLIENTE")},
            inverseJoinColumns
                    = {@JoinColumn(name = "ID_LINHA")})
    private List<Linha> linhas = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Linha> getLinhas() {
        return linhas;
    }

    public void pushLinhas(Linha... linhas) {
        this.linhas.addAll(Arrays.asList(linhas));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return cpf.equals(cliente.cpf);
    }
}
